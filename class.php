<?php
    class Htmlpage {
        protected $title = "Exercise 3";
        protected $body = "Naama Mamo";
        function __construct($title ="",$body ="") {
            if ($title !=""){
                $this->title=$title;
            }
            if ($body !=""){
                $this->body=$body;
            }
         }         
        public function view() {
            echo "<html>
                <head>
                <title>
                    $this->title
                </title></head>
                <body>
                     $this->body
                </body>
                </html>";
        }  
    }
    class ColoredMessage extends Htmlpage {
        protected $color = 'black';
        public function __set($property,$value){
        if ($property == 'color'){
            $colors = array('green','blue','gray');
            if(in_array($value, $colors)){
                $this->color = $value;
          } else{
                    $this->body = 'invalid color';
          }
        }
      }

        public function show(){
            echo "<p style = 'color:$this->color'>$this->body</p>";

    }
  }
  
    class Fontsize extends ColoredMessage {
        protected $size;
        public function __set($property,$value){
            if ($property == 'size'){
                $sizes=range(10,24);
                if(in_array($value, $sizes)){
                    $this->size = $value;
                }elseif($this->body == 'invalid color'){
                    $this->body = 'invalid color and font size';
                } else{
                    $this->body = 'invalid font size';
                }
            }
            elseif ($property == 'color'){
                parent::__set($property,$value);
            }
        }

        public function show(){
            echo "<p style = 'color:$this->color;font-size:$this->size'>$this->body</p>";
            }
    }          

?>
